﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="Sol_CheckBox.AddUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <style>
        body {
            padding: 10px;
            margin: auto;
        }

        .divCenterPosition {
            margin: 0px auto;
            margin-top: 130px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <div class="divCenterPosition" style="width: 20%">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" runat="server" AccessKey="f" AssociatedControlID="txtFirstName">
                                    <u>F</u>irstName
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" data-bind="value: firstName"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server" AccessKey="l" AssociatedControlID="txtLastName">
                                    <u>L</u>astName
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkonSite" Text="Onsite" runat="server" OnCheckedChanged="chkonSite_CheckedChanged" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                </td>
                            </tr>
                            </table>
                            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
