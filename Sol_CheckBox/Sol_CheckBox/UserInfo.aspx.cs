﻿using Sol_CheckBox.DAL;
using Sol_CheckBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBox
{
    public partial class UserInfo : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {

                await this.BindUserIdToDropdownList();
            }

        }

        protected async void btnBind_Click(object sender, EventArgs e)
        {
           await this.BindUserData();
        }

        protected async void btnEdit_Click(object sender, EventArgs e)
        {
            await this.EditUserData();
        }

        #region private method

        //Bind Data to DropDown List.
        private async  Task BindUserIdToDropdownList()
        {
            var getUserId = await new UserDal().UserSelectAllData(null);

            ddlUserId.DataSource = getUserId;
            ddlUserId.DataBind();

            ddlUserId.AppendDataBoundItems = true;
            ddlUserId.Items.Insert(0, new ListItem("-- Select UserId --", "0"));
            ddlUserId.SelectedIndex = 0;

        }


        // Bind userData based on selected UserID
        private async Task BindUserData()
        {
            //txtFirstName.Enabled = true;
            //txtLastName.Enabled = true;

            UserEntity userEntityObj = new UserEntity()
            {
                UserId= Convert.ToDecimal(ddlUserId.SelectedValue)
            };

            UserEntity userEntityObj1 =await new UserDal().UserSearchById(userEntityObj);

            txtFirstName.Text = userEntityObj1.FirstName;
            txtLastName.Text = userEntityObj1.LastName;
            chkonSite.Checked = Convert.ToBoolean(userEntityObj1.OnSite);

        }

        // Edit user Data 
        private async Task EditUserData()
        {
            txtFirstName.Enabled = true;
            txtLastName.Enabled = true;
            chkonSite.Enabled = true;

            var userObj = new UserEntity()
            {
                UserId = Convert.ToDecimal(ddlUserId.SelectedItem.Value),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                OnSite = chkonSite.Checked
            };

            var result = await new UserDal()?.UserUpdateData(userObj);
        }
        #endregion


    }
}