﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="Sol_CheckBox.UserInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        body {
            padding: 10px;
            margin: auto;
        }

        .divCenterPosition {
            margin: 0px auto;
            margin-top: 130px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <div class="divCenterPosition" style="width: 20%">
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlUserId" runat="server" DataTextField="UserId" DataValueField="UserId"></asp:DropDownList>

                                    <asp:Button ID="btnBind" Text="Bind" runat="server" OnClick="btnBind_Click" />
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkonSite" Text="Onsite" runat="server" Enabled="false" />
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
