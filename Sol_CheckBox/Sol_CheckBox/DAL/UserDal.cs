﻿using Sol_CheckBox.DAL.ORD;
using Sol_CheckBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_CheckBox.DAL
{
    public class UserDal
    {
        #region declaration

        private UserDcDataContext dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dc = new UserDcDataContext();
        }
        #endregion

        #region public Method
        public async Task<dynamic> UserAddData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                    dc
                    ?.uspSetUsers
                    (
                        "Insert",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        userEntityObj?.OnSite,
                        ref status,
                        ref message
                     );
                    return (status == 1) ? (dynamic)message : false;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<dynamic> UserUpdateData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                    dc
                    ?.uspSetUsers
                    (
                        "Updae",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        userEntityObj?.OnSite,
                        ref status,
                        ref message
                     );
                    return (status == 1) ? (dynamic)message : false;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserEntity> UserSearchById(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dc
                    ?.uspGetUsers
                    (
                        "SearchById",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        userEntityObj?.OnSite,
                        ref status,
                        ref message
                     )
                     ?.AsEnumerable()
                     ?.Select((leUserGetResult) => new UserEntity()
                     {
                         FirstName = leUserGetResult?.FirstName,
                         LastName = leUserGetResult?.LastName,
                         OnSite = leUserGetResult?.OnSite
                     })?.FirstOrDefault();

                    return getQuery;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<UserEntity>> UserSelectAllData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dc
                    ?.uspGetUsers
                    (
                        "SelectAllData",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        userEntityObj?.OnSite,
                        ref status,
                        ref message
                     )
                     ?.AsEnumerable()
                     ?.Select((lelinqUserGetResult) => new UserEntity()
                     {
                         UserId = lelinqUserGetResult.UserId,
                         //FirstName=lelinqUserGetResult.FirstName,
                         //LastName=lelinqUserGetResult.LastName,
                         //OnSite=lelinqUserGetResult.OnSite
                     })?.ToList();

                    return getQuery;
                });
            }

            catch (Exception)
            {
                throw;
            }
            #endregion
        }
    }
}