﻿using Sol_CheckBox.DAL;
using Sol_CheckBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBox
{
    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void chkonSite_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var userObj = new UserEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    OnSite = chkonSite.Checked
                };
                var result = new UserDal()?.UserAddData(userObj);
            }
            catch(Exception)
            {
                throw;
            }
           
        }

       
    }
}
