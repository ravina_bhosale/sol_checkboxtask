﻿CREATE TABLE [dbo].[tblUsers] (
    [UserId]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    [OnSite]    BIT          NULL,
    CONSTRAINT [PK_tblUsers] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

