﻿CREATE PROCEDURE uspSetUsers
(
	@Command Varchar(MAX)=NULL,
	
	@UserId numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@OnSite bit,

	@Status int= NULL OUT,
	@Message Varchar(MAX)=NULL OUT
)
AS
	
	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Insert'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
					INSERT INTO tblUsers
					(
						FirstName,
						LastName,
						OnSite
					)
					VALUES
					(
						@FirstName,
						@LastName,
						@OnSite
					)

					SET @Status=1
					SET @Message='Insert Success'

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END 

			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

						SELECT 
						@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
						@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END
						FROM tblUsers AS U
							WHERE U.UserId=@UserId

							UPDATE tblUsers
								SET 
									FirstName=@FirstName,
									LastName=@LastName,
									OnSite=@OnSite
										WHERE UserId=@UserId


								SET @Status=1
								SET @Message='Update Succesfully'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


			END
	END 
