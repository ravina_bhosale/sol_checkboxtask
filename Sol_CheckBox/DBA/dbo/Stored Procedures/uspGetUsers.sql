﻿CREATE PROCEDURE uspGetUsers
(
	@Command Varchar(50)=NULL,
	
	@UserId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@OnSite bit,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
)

AS

		DECLARE @ERRORMESSAGE VARCHAR(MAX)

		IF @Command='SearchById'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT
					U.UserId,
					U.FirstName,
					U.LastName,
					U.OnSite
					FROM tblUsers AS U
						WHERE U.UserId=@UserId

				SET @Status=1
				SET @Message='Get search Data'

			COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
						
				SET @Status=0
				SET @Message='Search Exception'

				RAISERROR(@ErrorMessage,16,1)
			END CATCH
		END
		ELSE IF @Command='SelectAllData'
		BEGIN
		BEGIN TRANSACTION

			BEGIN TRY

				SELECT
					U.UserId,
					U.FirstName,
					U.LastName,
					U.OnSite
					FROM tblUsers AS U

				SET @Status=1
				SET @Message='Select Succssfully'

			COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION
						
				SET @Status=0
				SET @Message='Select Exception'

				RAISERROR(@ErrorMessage,16,1)
			END CATCH
		END

